using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnemyHealth : MonoBehaviour, IHealth
{
    [SerializeField]
    protected int currentHealth;
    public int CurrentHealth { get { return currentHealth; } }

    [SerializeField]
    protected int maxHealth;
    public int MaxHealth { get { return maxHealth; } }

    //slider -- uses UI package
    [SerializeField]
    private Slider enemyHealthSlider;
    void Start()
    {
        currentHealth = maxHealth;
    }
    void Update()
    {
        if (enemyHealthSlider != null)
        {
            //sets position of health bar + offset
            enemyHealthSlider.transform.position = Camera.main.WorldToScreenPoint(transform.position + new Vector3(0, (float)1, 0));
        }
    }

    public void Die()
    {
        // would be good to do some death animation here maybe
        // remove this object from the game
        Destroy(gameObject);
    }

    public void Heal(int healingAmount)
    {
        currentHealth += healingAmount;
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
    }
    /**
     * Updates Enemy health slider GUI
     * @Param takes in a float and that updates the slider to a percentage value 
     */
    public void UpdateEnemyHealthSlider(float sliderValue)
    {
        if (enemyHealthSlider != null)
        {
            enemyHealthSlider.value = sliderValue;
        }

    }
    public void TakeDamage(int damageAmount)
    {
        currentHealth -= damageAmount;
        UpdateEnemyHealthSlider((float)currentHealth / (float)maxHealth);

        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Die();
        }
    }

}
