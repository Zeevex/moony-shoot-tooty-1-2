﻿using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour
{

    // SerializeField exposes this value to the Editor, but not to other Scripts!
    // It is "pseudo public"
    // HorizontalPlayerSpeed indicates how fast we accelerate Horizontally
    private EngineBase movement;// = new EngineBase();
    // SerializeField exposes this value to the Editor, but not to other Scripts!
    // It is "pseudo public"
    // HorizontalPlayerSpeed indicates how fast we ac

    // Use this for initialization
    private float horizontalInput = 0f, verticalInput = 0f;
    void Start()
    {
        // Get OurRigidbodyComponent once at the start of the game and store a reference to it
        // This means that we don't need to call GetComponent more than once! This makes our game faster. (GetComponent is SLOW)
        movement = GetComponent<EngineBase>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
    }
    private void FixedUpdate()
    {

        if (horizontalInput != 0.0f)
        {
            movement.Accelerate(Vector2.right * horizontalInput); // Using the movement calculations in EngineBase

            //print(HorizontalInput);
        }
        if (verticalInput != 0.0f)
        {
            movement.Accelerate(Vector2.up * verticalInput); // Using the movement calculations in EngineBase

            print(verticalInput);
        }
    }
}
