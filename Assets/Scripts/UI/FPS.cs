﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class FPS : MonoBehaviour
{

    [SerializeField]
    private Text fpsText;

    private float minFPS = 1000.0f;
    private float maxFPS = 0.0f;

    private Queue<float> frameQueue = new Queue<float>();

    private float aveFPSCounter = 0;
    private float aveFPS;

    // Use this for initialization
    void Start()
    {
        fpsText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        // After some time, start showing FPS
        // We do this so we don't accidentally tag the lowest FPS while initializing
        if (Time.realtimeSinceStartup > 5.0f)
        {
            float curFPS = (1.0f / Time.deltaTime);

            frameQueue.Enqueue(Time.deltaTime);

            if (frameQueue.Count > 60)
            {
                frameQueue.Dequeue();
            }

            // average for last 60 frames
            float averageDeltaTime = frameQueue.Average();
            float movingAverageFPS = (1.0f / averageDeltaTime);

            // Calculate full game average
            aveFPSCounter++;
            aveFPS = (aveFPS * ((aveFPSCounter - 1) / aveFPSCounter)) + (curFPS / aveFPSCounter);

            fpsText.text = string.Format("Ave FPS:  {0}", Mathf.RoundToInt(aveFPS).ToString());

            minFPS = Mathf.Min(minFPS, movingAverageFPS);
            maxFPS = Mathf.Max(maxFPS, movingAverageFPS);

            fpsText.text += string.Format("\nMin\\Max:  {0}\\{1}", Mathf.RoundToInt(minFPS).ToString(), Mathf.RoundToInt(maxFPS).ToString());
            fpsText.text += string.Format("\nTime Played:  {0}", (Time.realtimeSinceStartup).ToString());

            //fpsText.text += string.Format("\nMin FPS:  {0}", Mathf.RoundToInt(minFPS).ToString());
            //fpsText.text += string.Format("\nMax FPS:  {0}", Mathf.RoundToInt(maxFPS).ToString());
        }
    }
}
