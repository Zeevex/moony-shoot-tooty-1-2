﻿using UnityEngine;
using System.Collections;
using System;

public class MoveForwardConstantly: MonoBehaviour
{

    [SerializeField]
    private float acceleration = 100f;

    [SerializeField]
    private float initialVelocity = 10f;
    private Rigidbody2D ourRigidbody;

    // Because we're in a Pool, and Start and Awake only ever get called once, we'll use OnEnable instead!
    // @TODO: Verify that we can't GetComponent<Rigidbody2D> in Start or Awake instead;
    private void Awake()
    {
        ourRigidbody = GetComponent<Rigidbody2D>();
    }
    public void OnEnable()
    {
        
        ourRigidbody.velocity = transform.up * initialVelocity;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 forceToAdd = transform.up * acceleration * Time.deltaTime;
        

        ourRigidbody.AddForce(forceToAdd);
    }
}
