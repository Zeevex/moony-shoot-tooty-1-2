﻿using UnityEngine;
using System.Collections;

public class EnemyCollisionScript : MonoBehaviour, IUsePool
{
    private static Score score = null;

    public void Start()
    {
        if (score == null)
        {
            score = Object.FindObjectOfType<Score>();
        }
    }

    private ObjectPool objectPool;

    public void SetPool(ObjectPool givenObjectPool)
    {
        objectPool = givenObjectPool;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        // Enemies are only destroyed by PlayerProjectile
        if (other.tag == "PlayerProjectile")
        {
            // @TODO: Play explosion animation (?)
            // Add to the score
            // @TODO: When we add different enemy types, give them different values!
            //Bosses are worth 3 points, standard 1 and asteroids are untouched.
            if(this.gameObject.name == "EnemyBoss")
            {
                score.AddScore(3);
            }
            else
            {
                score.AddScore(1);
            }
            

            if (objectPool != null)
            {
                objectPool.returnObject(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
